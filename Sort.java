import java.util.Arrays;

public class Sort {
    public static void sortFloatArr(float[] arr) {

        for (int j = 1; j < arr.length; j++)  {
            float key = arr[j];
            int i = j - 1;
            while ((i > -1) && (arr[i] > key)) {
                arr[i + 1] = arr[i];
                i--;
            }
            arr[i + 1] = key;
        }
    }

    public static void sortCharArr(char[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            int index = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[index]) {
                    index = j;
                }
            }
            char smallerChar = arr[index];
            arr[index] = arr[i];
            arr[i] = smallerChar;
        }
    }

    public static double[] sortDoubleArr(double[] arr) {
        boolean s = false;
        double bat;
        while (!s) {
            s = true;
            for (int i = 0; i < arr.length - 1; i++) {
                if (arr[i] > arr[i + 1]) {
                    s = false;

                    bat = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = bat;
                }
            }

        }
        return arr;
    }


    public static void sortStringArr(String[] arr) {
        boolean s = false;
        String bat;
        while (!s) {
            s = true;
            for (int i = 0; i < arr.length - 1; i++) {
                if (arr[i].compareTo(arr[i + 1]) > 0) {
                    s = false;

                    bat = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = bat;
                }
            }

        }

    }

    public static int[] sortIntArr(int[] arr) {
        boolean s = false;
        int bat;
        while (!s) {
            s = true;
            for (int i = 0; i < arr.length - 1; i++) {
                if (arr[i] > arr[i + 1]) {
                    s = false;

                    bat = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = bat;
                }
            }

        }
        return arr;
    }

    public static void main(String[] args) {
        int[] arr = {2, 5, 4, 85, 49};//bubble sorting
        arr = sortIntArr(arr);
        System.out.println(Arrays.toString(arr));
        String[] stringArr = {"Minsk", "Vilnus", "Marokko", "Pinsk", "Berlin"};// bubble sorting
        sortStringArr(stringArr);
        System.out.println(Arrays.toString(stringArr));
        double[] doubleArr = {2, 5, 4, 85, 49};//bubble sorting
        doubleArr = sortDoubleArr(doubleArr);
        System.out.println(Arrays.toString(doubleArr));
        char[] charArr = {'d', 'w', 'l', 'o', 'n'};//selection sorting
        sortCharArr(charArr);
        System.out.println(Arrays.toString(charArr));
        float[] floatArr = {2.1f, 9.22f, 0.2f, 5.3f};//insertion sorting
        sortFloatArr(floatArr);
        System.out.println(Arrays.toString(floatArr));
    }
}



